package com.excellentsoftwares.mychatapp;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Groups extends Fragment {

    ListView grouplistview;
    List<GroupModel> gList;
    private DatabaseReference groupDatabase;
    private ProgressDialog mLoginProgress;
    public static String groupId,groupName,groupPicUrl;
    public Groups() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_groups, container, false);
        gList = new ArrayList<>();
        groupDatabase = FirebaseDatabase.getInstance().getReference("Groups");
        grouplistview = view.findViewById(R.id.grouplistview);
        mLoginProgress = new ProgressDialog(getActivity());
        conversationPage();


     return view;
    }
    @Override
    public void onStart() {
        super.onStart();

        mLoginProgress.setTitle("Plz Wait");
        mLoginProgress.setMessage("Connecting...");
        mLoginProgress.setCanceledOnTouchOutside(false);
        mLoginProgress.show();
        groupDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                gList.clear();

                for (DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                    GroupModel model = usersnapshot.getValue(GroupModel.class);

                        gList.add(model);
                    Group_Adapter adapter = new Group_Adapter(getActivity(),gList);
                    grouplistview.setAdapter(adapter);


                }
                mLoginProgress.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void conversationPage() {

        grouplistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                GroupModel item = gList.get(i);
                String itemId = item.getGroupId();
                String namegroup = item.getGroupName();
                String itemUrl = item.getPicUrl();

                SharedPreferences list = getActivity().getSharedPreferences("groupDetails",
                        Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = list.edit();
                editor.putString("id", itemId);
                editor.putString("name", namegroup);
                editor.putString("url",itemUrl);
                editor.commit();


                Intent intent = new Intent(getActivity(),ConversationPage.class);
                intent.putExtra(groupId,itemId);
                intent.putExtra(groupName,namegroup);
                startActivity(intent);

            }
        });

    }


}
