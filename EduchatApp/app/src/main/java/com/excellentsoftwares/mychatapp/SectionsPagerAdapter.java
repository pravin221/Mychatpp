package com.excellentsoftwares.mychatapp;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

class SectionsPagerAdapter extends FragmentPagerAdapter{


    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch(position) {
            case 0:
                Groups groupFrragment = new Groups();
                return groupFrragment;

            case 1:
                People peopleFragment = new People();
                return  peopleFragment;


            default:
                return  null;
        }

    }

    @Override
    public int getCount() {
        return 2;
    }

    public CharSequence getPageTitle(int position){

        switch (position) {
            case 0:
                return "Groups";

            case 1:
                return "People";

            default:
                return null;
        }

    }

}
