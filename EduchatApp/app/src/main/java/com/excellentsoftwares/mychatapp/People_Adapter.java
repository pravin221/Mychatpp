package com.excellentsoftwares.mychatapp;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class People_Adapter extends ArrayAdapter<PeopleModel> {

    private Activity context;
    List<PeopleModel> pList;

    public People_Adapter(Activity context, List<PeopleModel> pList) {
        super(context, R.layout.people_adapter, pList);

        this.context = context;
        this.pList = pList;

    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.people_adapter, null, true);


        PeopleModel peopleModel = pList.get(position);

        TextView pTitle = listViewItem.findViewById(R.id.personTitle);

        pTitle.setText(peopleModel.getName());
        return listViewItem;

    }
}

