package com.excellentsoftwares.mychatapp;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class People extends Fragment {

    private ListView peopleListview;
    private DatabaseReference userDatabase;
    List<PeopleModel> pList;
    private ProgressDialog mLoginProgress;


    public People() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_people, container, false);

        peopleListview = view.findViewById(R.id.peopleListview);
        pList = new ArrayList<>();
        userDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mLoginProgress = new ProgressDialog(getActivity());
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();

        mLoginProgress.setTitle("Plz Wait");
        mLoginProgress.setMessage("Connecting...");
        mLoginProgress.setCanceledOnTouchOutside(false);
        mLoginProgress.show();
        userDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                pList.clear();

                for (DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                    PeopleModel model = usersnapshot.getValue(PeopleModel.class);

                    pList.add(model);
                    People_Adapter adapter = new People_Adapter(getActivity(),pList);
                    peopleListview.setAdapter(adapter);




                }
                mLoginProgress.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



}
