package com.excellentsoftwares.mychatapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder>{

    private List<Messages> mMessageList;
    private DatabaseReference mUserDatabase;
    private FirebaseAuth mAuth;
    private String uId;

    public MessageAdapter(List<Messages> mMessageList) {

        this.mMessageList = mMessageList;

    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_single_layout ,parent, false);

        return new MessageViewHolder(v);

    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {

        public TextView messageText,messageTextMe,timeText;
        public CircleImageView profileImage;
        public TextView displayName;
        public ImageView messageImage;

        public MessageViewHolder(View view) {
            super(view);

            messageText = (TextView) view.findViewById(R.id.message_text_layout);
            messageTextMe = (TextView) view.findViewById(R.id.message_text_me);
            profileImage = (CircleImageView) view.findViewById(R.id.message_profile_layout);
            displayName = (TextView) view.findViewById(R.id.name_text_layout);
            messageImage = (ImageView) view.findViewById(R.id.message_image_layout);
            timeText = (TextView) view.findViewById(R.id.time_text_layout);


//            DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(id);
//            ValueEventListener eventListener = new ValueEventListener() {
//                @Override
//                public void onDataChange(DataSnapshot dataSnapshot) {
//
//                    uId = dataSnapshot.child("id").getValue(String.class);
//
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//
//                }
//            };

           // databaseReference.addValueEventListener(eventListener);


        }
    }

    @Override
    public void onBindViewHolder(final MessageViewHolder viewHolder, int i) {

        viewHolder.setIsRecyclable(false);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String id = user.getUid();

        final Messages c = mMessageList.get(i);

        String message_type = c.getType();
        String userId = c.getuId();
        String textType = "Text";

        if (message_type.equals(textType)) {

            if (userId.equals(user.getUid())) {
                viewHolder.messageTextMe.setText(c.getMessage());
                viewHolder.messageText.setVisibility(View.INVISIBLE);
                viewHolder.displayName.setVisibility(View.INVISIBLE);
                viewHolder.profileImage.setVisibility(View.INVISIBLE);
                }
                else {
                Toast.makeText(viewHolder.messageText.getContext(), "other", Toast.LENGTH_SHORT).show();
                viewHolder.messageTextMe.setVisibility(View.INVISIBLE);
                viewHolder.displayName.setText(c.getUserName());
                viewHolder.messageText.setText(c.getMessage());
            }
            }
//            if
//            else {
//
//            Picasso.with(viewHolder.messageImage.getContext()).load(c.getMessage()).networkPolicy(NetworkPolicy.OFFLINE)
//                    .placeholder(R.drawable.default_avatar).into(viewHolder.messageImage, new Callback() {
//                @Override
//                public void onSuccess() {
//
//                }
//
//                @Override
//                public void onError() {
//
//                    Picasso.with(viewHolder.profileImage.getContext()).load(c.getMessage()).placeholder(R.drawable.default_avatar).into(viewHolder.messageImage);
//
//                }
//            });
            }

//         else {
//
//            viewHolder.messageText.setVisibility(View.INVISIBLE);
//            viewHolder.messageText.setText("unavailable");
//            viewHolder.messageImage.setVisibility(View.INVISIBLE);
//            viewHolder.displayName.setText(c.getUserName());
//            Picasso.with(viewHolder.messageImage.getContext()).load(c.getMessage()).networkPolicy(NetworkPolicy.OFFLINE)
//                    .placeholder(R.drawable.default_avatar).into(viewHolder.messageImage, new Callback() {
//                @Override
//                public void onSuccess() {
//
//                }
//
//                @Override
//                public void onError() {
//
//                    Picasso.with(viewHolder.profileImage.getContext()).load(c.getMessage()).placeholder(R.drawable.default_avatar).into(viewHolder.messageImage);
//                }
//            });
////            viewHolder.messageText.setVisibility(View.INVISIBLE);
////            Picasso.with(viewHolder.profileImage.getContext()).load(c.getMessage())
////                    .placeholder(R.drawable.default_avatar).into(viewHolder.messageImage);
//        }

    @Override
    public int getItemCount() {
        return mMessageList.size();

    }

}