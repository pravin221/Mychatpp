package com.excellentsoftwares.mychatapp;

public class PeopleModel {
    public String id;
    public String name;
    public String device_token;

    public PeopleModel(){

    }

    public PeopleModel(String id,String name, String device_token) {
        this.id = id;
        this.name = name;
        this.device_token = device_token;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}
