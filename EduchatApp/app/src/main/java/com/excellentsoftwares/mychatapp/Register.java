package com.excellentsoftwares.mychatapp;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;

public class Register extends AppCompatActivity {
    private EditText mDisplayName;
    private TextInputLayout mEmail;
    private TextInputLayout mPassword;
    private ImageButton mCreateBtn,enterReg;
    private String mobileNum;
    private TextView loginLabel;
    private Toolbar mToolbar;
    private DatabaseReference mDatabase;

    //ProgressDialog
    private ProgressDialog mRegProgress;
    private String display_name;

    //Firebase Auth
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mToolbar = (Toolbar) findViewById(R.id.register_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("EduChat");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mRegProgress = new ProgressDialog(this);


        // Firebase Auth

        mAuth = FirebaseAuth.getInstance();


        // Android Fields

        loginLabel = findViewById(R.id.login_label);
        mDisplayName = (EditText)findViewById(R.id.name);
        mEmail = (TextInputLayout) findViewById(R.id.register_email);
        mPassword = (TextInputLayout) findViewById(R.id.reg_password);
        mCreateBtn = (ImageButton) findViewById(R.id.reg_create_btn);
        enterReg = (ImageButton) findViewById(R.id.enterBt);

        mCreateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                mobileNum = mEmail.getEditText().getText().toString();
                String password = "12345678";
                String email = mobileNum + "@gmail.com";

                    mRegProgress.setTitle("Registering User");
                    mRegProgress.setMessage("Please wait while we create your account !");
                    mRegProgress.setCanceledOnTouchOutside(false);
                    mRegProgress.show();
                    register_user(email, password);
            }
        });


    }

    private void register_user(final String email, final String password) {

        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.getException() instanceof FirebaseAuthUserCollisionException) {
                    loginUser(email,password);

                } else if (task.isSuccessful()) {
                    loginLabel.setText("Plz Enter Your Name.");
                    mDisplayName.setVisibility(View.VISIBLE);
                    enterReg.setVisibility(View.VISIBLE);
                    FirebaseUser current_user = FirebaseAuth.getInstance().getCurrentUser();
                    final String uid = current_user.getUid();

                    mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);

                    final String device_token = FirebaseInstanceId.getInstance().getToken();

//                    final HashMap<String, String> userMap = new HashMap<>();
//                    userMap.put("id", uid);
//                    userMap.put("name", display_name);
//                    userMap.put("mobile", mobileNum);
//                    userMap.put("device_token", device_token);


                    mEmail.setVisibility(View.GONE);
                    mCreateBtn.setVisibility(View.GONE);
                    mRegProgress.dismiss();
                    enterReg.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            display_name = mDisplayName.getText().toString();
                            PeopleModel model = new PeopleModel(uid,display_name,device_token);
                            if (!TextUtils.isEmpty(mDisplayName.getText().toString())) {
                                mDatabase.setValue(model).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                        if (task.isSuccessful()) {

                                            Intent mainIntent = new Intent(Register.this, MainActivity.class);
                                            mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(mainIntent);
                                            finish();

                                        }

                                    }
                                });
                            }
                            else {
                                Toast.makeText(Register.this, "Plz enter your name", Toast.LENGTH_SHORT).show();
                            }
                        }

                    });




                } else {

                    mRegProgress.hide();
                    Toast.makeText(Register.this, "Cannot Sign in. Please check the form and try again.", Toast.LENGTH_LONG).show();

                }

            }
        });

    }

    public void loginUser(String email, String password) {


        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()) {


                    String current_user_id = mAuth.getCurrentUser().getUid();
                    String deviceToken = FirebaseInstanceId.getInstance().getToken();

                    Intent mainIntent = new Intent(Register.this, MainActivity.class);
                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(mainIntent);
                    finish();


                } else {


                    String task_result = task.getException().getMessage().toString();

                    Toast.makeText(Register.this, "Error : " + task_result, Toast.LENGTH_LONG).show();

                }

            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null){
            Intent i = new Intent(Register.this,MainActivity.class);
            startActivity(i);
        }

    }

}

