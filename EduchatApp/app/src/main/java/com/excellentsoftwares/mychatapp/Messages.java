package com.excellentsoftwares.mychatapp;

public class Messages {
    public String id,message, type;
    public String uId,userName;


    public Messages(){

    }

    public Messages(String id,String message, String type, String uId, String userName) {
        this.id = id;
        this.message = message;
        this.type = type;
        this.uId = uId;
        this.userName = userName;
    }

    public String getMessage() {
        return message;
    }

    public String getType() {
        return type;
    }

    public String getuId() {
        return uId;
    }

    public String getUserName() {
        return userName;
    }
}