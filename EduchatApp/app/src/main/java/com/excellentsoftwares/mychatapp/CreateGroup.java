package com.excellentsoftwares.mychatapp;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class CreateGroup extends AppCompatActivity {

    private EditText groupName;
    private Button createGroup;
    private DatabaseReference groupDatabase;
    private ListView selectListview;
    private DatabaseReference userDatabase;
    List<PeopleModel> pList;
    private ProgressDialog mLoginProgress;
    ArrayList<String> pIds;
    private People_Adapter adapter;
    private CircleImageView groupPic;
    private Uri filepath;
    private final int PICK_IMAGE_REQUEST = 1;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private ProgressDialog mProgressDialog;
    private StorageReference mImageStorage;
    private String gId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);

        selectListview = findViewById(R.id.choosepeopleListview);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        groupPic = findViewById(R.id.grouppic);
        groupName = findViewById(R.id.etGname);
        createGroup = findViewById(R.id.creategroup);

        groupDatabase = FirebaseDatabase.getInstance().getReference("Groups");
        gId = groupDatabase.push().getKey();
        createGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addGroup();
            }
        });
        pList = new ArrayList<>();
        userDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mLoginProgress = new ProgressDialog(this);
        adapter = new People_Adapter(CreateGroup.this, pList);
        selectListview.setAdapter(adapter);

        selectListview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        pIds = new ArrayList<>();

        selectListview.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean b) {
                PeopleModel selectedItem = pList.get(i);
                Toast.makeText(CreateGroup.this, "" + selectedItem.getName(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {


                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {

            }


        });

        groupPic.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
            @Override
            public void onClick(View view) {

                chooseImage();
            }
        });

        createGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addGroup();
            }
        });


        mProgressDialog = new ProgressDialog(this);
    }



    @Override
    public void onStart() {
        super.onStart();

        mLoginProgress.setTitle("Plz Wait");
        mLoginProgress.setMessage("Connecting...");
        mLoginProgress.setCanceledOnTouchOutside(false);
        mLoginProgress.show();
        userDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                pList.clear();

                for (DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                    PeopleModel model = usersnapshot.getValue(PeopleModel.class);

                    pList.add(model);


                }
                mLoginProgress.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

//    private void uploadImage() {
//        if (filepath!=null){
//            ProgressDialog progressDialog = new ProgressDialog(this);
//            progressDialog.setMessage("Uploading");
//            StorageReference reference = storageReference.child("images").child(gId);
//            reference.putFile(filepath)
//                    .addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
//                        @Override
//                        public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
//                            Toast.makeText(CreateGroup.this, "Uploaded", Toast.LENGTH_SHORT).show();
//                        }
//                    });
//        }
//    }

    private void chooseImage() {
        Intent intent = new Intent();
        intent.setType("image/+");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Select Picture"),PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode  == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data!= null && data.getData() != null)
        {

            filepath = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),filepath);
                groupPic.setImageBitmap(bitmap);
            }
            catch (IOException e){
                e.printStackTrace();

            }
        }

    }

    private void addGroup() {
        final String gName = groupName.getText().toString();
        StorageReference reference = storageReference.child("images").child(gId);


        reference.putFile(filepath).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                if (task.isSuccessful()){

                   final String downloadUrl = task.getResult().getDownloadUrl().toString();
                    GroupModel model = new GroupModel(gId, gName,downloadUrl);
                    groupDatabase.child(gId).setValue(model);

                }
                else {

                    Toast.makeText(CreateGroup.this, "Error in uploading thumbnail.", Toast.LENGTH_LONG).show();
                    mProgressDialog.dismiss();
                }

            }
        });
        Intent intent = new Intent(CreateGroup.this, MainActivity.class);
        startActivity(intent);

    }


  }
