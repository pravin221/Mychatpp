package com.excellentsoftwares.mychatapp;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Group_Adapter extends ArrayAdapter<GroupModel> {

    private Activity context;
    List<GroupModel> gList;
    private FirebaseStorage storage;
    private StorageReference storageReference;

    public Group_Adapter(Activity context, List<GroupModel> gList) {
        super(context, R.layout.group_adapter, gList);

        this.context = context;
        this.gList = gList;

    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        View listViewItem = inflater.inflate(R.layout.group_adapter, null, true);
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        GroupModel groupModel = gList.get(position);

            TextView gTitle = listViewItem.findViewById(R.id.groupTitle);
        final CircleImageView gImage = listViewItem.findViewById(R.id.groupidImage);

            gTitle.setText(groupModel.groupName);
            final String imageurl = groupModel.getPicUrl();
        Picasso.with(getContext()).load(imageurl).networkPolicy(NetworkPolicy.OFFLINE)
                .placeholder(R.drawable.default_avatar).into(gImage, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {

                Picasso.with(getContext()).load(imageurl).placeholder(R.drawable.default_avatar).into(gImage);

            }
        });




            return listViewItem;
        }




}






