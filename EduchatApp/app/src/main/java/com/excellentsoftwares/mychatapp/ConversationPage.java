package com.excellentsoftwares.mychatapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ConversationPage extends AppCompatActivity {

    private Toolbar mToolbar;
    private String groupId,gName,groupImageUrl;
    private ImageButton mChatAddBtn,mChatsendBtn;
    private TextView mTitleView,mLastSeenView;
    private EditText mChatMessageView;
    private CircleImageView mProfileImage;
    private MessageAdapter messageAdapter;
    private DatabaseReference chatDatabase;
    private List<Messages>mList;
    private RecyclerView showMessages;
    private Uri filepath;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private FirebaseAuth mAuth;
    private LinearLayoutManager mLinearLayout;
    private String uId,uName;
    private static final int GALLERY_PICK = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation_page);

        mToolbar = (Toolbar) findViewById(R.id.chat_app_bar);

        //userInfo
//        SharedPreferences list = getSharedPreferences("userInfo",
//                Context.MODE_PRIVATE);
//         uId = list.getString("userId","defaulvalue");
//         uName = list.getString("userName","defaultvalue");



        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        mAuth = FirebaseAuth.getInstance();
        mList = new ArrayList<>();
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View action_bar_view = inflater.inflate(R.layout.chat_custom_bar, null);
        actionBar.setCustomView(action_bar_view);

        gName =getIntent().getStringExtra(Groups.groupName);

        // ---- Custom Action bar Items ----

        mTitleView = (TextView) findViewById(R.id.custom_bar_title);
        mProfileImage = (CircleImageView) findViewById(R.id.custom_bar_image);

        SharedPreferences gInfo =getSharedPreferences("groupDetails",
                Context.MODE_PRIVATE);
        final String imageUrl = gInfo.getString("url", "defaultvalue");
        final String id = gInfo.getString("id", "defaultvalue");

        chatDatabase = FirebaseDatabase.getInstance().getReference("Chat").child(id);

        mTitleView.setText(gName);
        Picasso.with(ConversationPage.this).load(imageUrl).networkPolicy(NetworkPolicy.OFFLINE)
                .placeholder(R.drawable.default_avatar).into(mProfileImage, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {

                Picasso.with(ConversationPage.this).load(imageUrl).placeholder(R.drawable.default_avatar).into(mProfileImage);

            }
        });



        showMessages = findViewById(R.id.messages_list);
        mChatMessageView = (EditText) findViewById(R.id.chat_message_view);
        mChatAddBtn = (ImageButton) findViewById(R.id.chat_add_btn);
        messageAdapter = new MessageAdapter(mList);
        mLinearLayout = new LinearLayoutManager(this);
        showMessages.setHasFixedSize(true);
        showMessages.setLayoutManager(mLinearLayout);
        mLinearLayout.setStackFromEnd(true);
        mChatsendBtn = (ImageButton)findViewById(R.id.chat_send_btn);
        mChatAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent galleryIntent = new Intent();
                galleryIntent.setType("image/*");
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(galleryIntent, "SELECT IMAGE"), GALLERY_PICK);

            }
        });
        mChatsendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
               // sendPic();
            }
        });



    }


        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode  == GALLERY_PICK && resultCode == RESULT_OK
                    && data!= null && data.getData() != null)
            {

                filepath = data.getData();

            }

        }

        private void sendPic(){
            final String mId = chatDatabase.push().getKey();
            StorageReference reference = storageReference.child("images").child(mId);

            reference.putFile(filepath).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                    if (task.isSuccessful()){

                        final String downloadUrl = task.getResult().getDownloadUrl().toString();
                        String type = "image";
                      //  Messages model = new Messages(mId,downloadUrl,type,uName);
                       // chatDatabase.child(mId).setValue(model);

                    }
                    else {

                        Toast.makeText(ConversationPage.this, "Error in uploading thumbnail.", Toast.LENGTH_LONG).show();

                    }

                }
            });


        }




    @Override
    protected void onStart() {
        super.onStart();
        getUserInfo();
        loadMessage();

    }

    private void sendMessage(){

        String message = mChatMessageView.getText().toString();
        String mId = chatDatabase.push().getKey();
        FirebaseUser user = mAuth.getCurrentUser();
        String id = user.getUid();

        if(!TextUtils.isEmpty(message)) {

            Messages messages = new Messages(mId,message,"text",id,uName);
            chatDatabase.child(mId).setValue(messages);

            mChatMessageView.setText("");
        }
        else {

            Toast.makeText(this, "Write message", Toast.LENGTH_SHORT).show();
        }


    }

    private void loadMessage(){
        chatDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mList.clear();

                for (DataSnapshot usersnapshot : dataSnapshot.getChildren()) {
                    String msg = usersnapshot.child("message").getValue(String.class);
                    String mId = dataSnapshot.child("id").getValue(String.class);
                    String userId =usersnapshot.child("uId").getValue(String.class);
                String userName = usersnapshot.child("userName").getValue(String.class);
                String type = "Text";
                Messages model = new Messages(mId,msg,type,userId,userName);
                    mList.add(model);
                    showMessages.setAdapter(messageAdapter);
                    messageAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

//        ValueEventListener eventListener = new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                String mId = dataSnapshot.child("id").getValue(String.class);
//                String msg = dataSnapshot.child("message").getValue(String.class);
//                String userName = dataSnapshot.child("userName").getValue(String.class);
//                Toast.makeText(ConversationPage.this, ""+msg, Toast.LENGTH_SHORT).show();
//                String type = "Text";
//                Messages model = new Messages(mId,msg,type,userName);
//              mList.add(model);
//                showMessages.setAdapter(messageAdapter);
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        };
//
//        chatDatabase.addValueEventListener(eventListener);
//    }

}

    private void getUserInfo(){

        FirebaseUser user = mAuth.getCurrentUser();
        String id = user.getUid();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(id);
        ValueEventListener eventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                uName = dataSnapshot.child("name").getValue(String.class);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        databaseReference.addValueEventListener(eventListener);
//
        SharedPreferences list = getSharedPreferences("userInfo",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = list.edit();
        editor.putString("userId",id);
        editor.putString("userName", uName);
        editor.commit();
    }
}
