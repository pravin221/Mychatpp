package com.excellentsoftwares.mychatapp;

public class GroupModel {

    public String groupId;
    public String groupName;
    public String picUrl;

    GroupModel(){

    }

    public GroupModel(String groupId, String groupName, String picUrl) {
        this.groupId = groupId;
        this.groupName = groupName;
        this.picUrl = picUrl;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public String getPicUrl() {
        return picUrl;
    }
}
